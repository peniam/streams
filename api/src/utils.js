const fs = require('fs');

let streams = fs.readFileSync('./db.json');
streams = JSON.parse(streams).streams;

function getStreams() {
	return streams;
}

function getStream(id) {
	const data = streams.filter(el => el.id === id);
	return data;
}

module.exports = {
	getStream,
	getStreams
};
