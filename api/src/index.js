const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');
const uuidv1 = require('uuid/v1');
const fs = require('fs');

const { getStream } = require('./utils');

const app = express();

app.use(bodyParser.json());
app.use(cors());

app.get('/streams', (req, res) => {
	const data = fs.readFileSync('./db.json');

	res.json(JSON.parse(data).streams);
});

app.get('/streams/:id', (req, res) => {
	const id = req.params.id;
	const stream = getStream(id);
	res.json(stream);
});

app.post('/streams', (req, res) => {
	const body = req.body;
	const id = uuidv1();
	const stream = { ...body, id };
	fs.readFile('./db.json', (err, data) => {
		if (err) throw err;
		let db = JSON.parse(data);
		db.streams.push(stream);
		db = JSON.stringify(db, null, 2);

		fs.writeFile('./db.json', db, err => {
			if (err) throw err;
			res.json(stream);
		});
	});
});
app.patch('/streams/:id', (req, res) => {
	const id = req.params.id;

	const body = { ...req.body };

	fs.readFile('./db.json', (err, data) => {
		if (err) throw err;
		let db = JSON.parse(data);
		const index = db.streams.findIndex(el => el.id === id);

		Object.assign(db.streams[index], body);

		const resp = db.streams[index];

		db = JSON.stringify(db, null, 2);

		fs.writeFile('./db.json', db, err => {
			if (err) throw err;

			res.json(resp);
		});
	});
});

app.delete('/streams/:id', (req, res) => {
	const id = req.params.id;

	fs.readFile('./db.json', (err, data) => {
		if (err) throw err;
		let db = JSON.parse(data);
		db.streams = db.streams.filter(el => el.id !== id);
		db = JSON.stringify(db, null, 2);

		fs.writeFile('./db.json', db, err => {
			if (err) throw err;
		});
	});

	res.send('Success!');
});

app.listen(5000, () => console.log('Server is listening on port: 5000'));

