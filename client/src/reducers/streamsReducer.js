import {
    FETCH_STREAMS_SUCCESS,
    FETCH_STREAM_SUCCESS,
    FETCH_LOADING,
    STREAMS_ERROR,
    CREATE_STREAM,
    DELETE_STREAM,
    EDIT_STREAM
} from '../actions/types'

const initialState = {
    data: {},
    loading: false,
    error: null
}

const streamsReducer = (state = initialState, action) => {
    switch (action.type) {
        case FETCH_STREAMS_SUCCESS:
            const data = action.payload.reduce((prev, next) => {
                prev[next.id] = next
                return prev
            }, {})

            return { ...state, data, loading: false, error: null }

        case FETCH_STREAM_SUCCESS:
            return {
                ...state,
                data: { [action.payload[0].id]: action.payload[0] },
                loading: false,
                error: null
            }
        case FETCH_LOADING:
            return { ...state, loading: true }
        case STREAMS_ERROR:
            return { ...state, loading: false, error: payload }

        case CREATE_STREAM:
            const createData = state.data
            createData[action.payload.id] = action.payload

            return {
                ...state,
                data: { ...createData },
                loading: false,
                error: null
            }
        case EDIT_STREAM:
            return {
                ...state,
                data: { ...state.data, [action.payload.id]: action.payload },
                loading: false,
                error: null
            }

        case DELETE_STREAM:
            const newData = state.data
            delete newData[action.payload]
            return {
                ...state,
                data: {
                    ...newData
                },
                loading: false,
                error: null
            }
        default:
            return state
    }
}

export default streamsReducer
