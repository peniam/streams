import {
    FETCH_STREAMS_SUCCESS,
    FETCH_LOADING,
    FETCH_STREAM_SUCCESS,
    STREAMS_ERROR,
    CREATE_STREAM,
    DELETE_STREAM,
    EDIT_STREAM
} from './types'
import api from '../api/api'
import history from '../history'

export const fetchStreams = () => async dispatch => {
    try {
        dispatch({ type: FETCH_LOADING })
        const response = await api.get('/streams')
        dispatch({
            type: FETCH_STREAMS_SUCCESS,
            payload: response.data
        })
    } catch (error) {
        dispatch({
            type: STREAMS_ERROR,
            payload: error.message || 'Something went wrong!'
        })
    }
}

export const fetchStream = id => async dispatch => {
    dispatch({ type: FETCH_LOADING })

    try {
        const res = await api.get(`/streams/${id}`)
        dispatch({
            type: FETCH_STREAM_SUCCESS,
            payload: res.data
        })
    } catch (error) {
        dispatch({
            type: STREAMS_ERROR,
            payload: error.message || 'Something went wrong!'
        })
    }
}

export const createStream = formValues => async (dispatch, getState) => {
    const userId = getState().auth.userId
    const data = { ...formValues, userId }
    try {
        const res = await api.post('/streams', data)
        dispatch({
            type: CREATE_STREAM,
            payload: res.data
        })
        history.push('/')
    } catch (error) {
        dispatch({
            type: STREAMS_ERROR,
            payload: error.message || 'Something went wrong!'
        })
    }
}

export const editStream = (id, data) => async dispatch => {
    try {
        const res = await api.patch(`/streams/${id}`, data)

        dispatch({
            type: EDIT_STREAM,
            payload: res.data
        })
        history.push('/')
    } catch (error) {
        dispatch({
            type: STREAMS_ERROR,
            payload: error.message || 'Something went wrong!'
        })
    }
}

export const deleteStream = id => async dispatch => {
    try {
        await api.delete(`/streams/${id}`)

        dispatch({
            type: DELETE_STREAM,
            payload: id
        })
        history.push('/')
    } catch (error) {
        dispatch({
            type: STREAMS_ERROR,
            payload: error.message || 'Something went wrong!'
        })
    }
}
