import React from 'react'
import classes from './Loader.module.css'

const Loader = () => {
    return (
        <div className={classes.container}>
            <h3>This page takes forever to load.</h3>
            <div className={classes.loader} />
        </div>
    )
}

export default Loader
