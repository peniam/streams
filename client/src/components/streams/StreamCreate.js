import React, { Component } from 'react'
import StreamForm from './StreamForm'
import { createStream } from '../../actions/streams'
import { connect } from 'react-redux'

class StreamCreate extends Component {
    onSubmit = formValues => {
        this.props.createStream(formValues)
    }
    render() {
        return (
            <div>
                <h1>Create Stream</h1>
                <StreamForm onSubmit={this.onSubmit} />
            </div>
        )
    }
}

export default connect(
    null,
    { createStream }
)(StreamCreate)
