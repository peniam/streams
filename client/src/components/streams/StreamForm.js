import React, { Component } from 'react'
import { Field, reduxForm } from 'redux-form'

class StreamForm extends Component {
    renderError({ error, touched }) {
        if (touched && error) {
            return (
                <div>
                    <div>{error}</div>
                </div>
            )
        }
    }

    renderInput = ({ input, label, meta }) => {
        const className = `field ${meta.error && meta.touched ? 'error' : ''}`
        return (
            <div className={className}>
                <label htmlFor=''>{label}</label>
                <input {...input} autoComplete='off' />
                {this.renderError(meta)}
            </div>
        )
    }

    onSubmit = formValues => {
        this.props.onSubmit(formValues)
    }
    render() {
        return (
            <form onSubmit={this.props.handleSubmit(this.onSubmit)}>
                <div>
                    <Field
                        name='title'
                        component={this.renderInput}
                        label='Enter Title'
                    />
                </div>
                <div>
                    <Field
                        name='description'
                        component={this.renderInput}
                        label='Enter Description'
                    />
                </div>

                <button>Submit</button>
            </form>
        )
    }
}

const validate = values => {
    const errors = {}
    if (!values.title) {
        errors.title = 'You must enter a title'
    }

    if (!values.description) {
        errors.description = 'You must enter a description'
    }
    return errors
}

StreamForm = reduxForm({
    form: 'streamForm',
    validate
})(StreamForm)

export default StreamForm
