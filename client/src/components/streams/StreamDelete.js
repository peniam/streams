import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import styled, { css } from 'styled-components'
import Modal from '../Modal'
import { connect } from 'react-redux'
import { deleteStream } from '../../actions/streams'

const Div = styled.div`
    position: fixed;
    top: 0;
    left: 0;
    right: 0;
    bottom: 0;
    background-color: rgba(0, 0, 0, 0.8);
    width: 100%;
    height: 100vh;
    display: flex;
    justify-content: center;
    align-items: center;

    & > div {
        width: 90vmin;
        height: 13em;
        background-color: #eee;
        padding: 1.5em;
        display: flex;
        flex-direction: column;
        justify-content: space-between;

        h2 {
            font-size: 1.6rem;
            color: brown;
            margin-bottom: 0.5em;
        }
        p {
            font-size: 1.3em;
            margin-bottom: auto;
        }

        div {
            margin-left: auto;
            margin-bottom: 1.5em;
        }
    }
`

const Button = styled(Link)`
    padding: 0.5em 1.5em;
    background-color: red;
    border-radius: 0.5em;
    font-size: 1.2rem;
    color: #fff;

    ${props =>
        props.cancel &&
        css`
            background-color: #ddd;
            margin-left: 1em;
            color: #222;
        `}
`

class StreamDelete extends Component {
    renderModal = () => {
        const { title, id } = this.props.stream
        return (
            <Div>
                <div>
                    <h2>Delete Stream</h2>
                    <p>
                        Are you sure you want to delete the stream with title:
                        {title}?
                    </p>
                    <div>
                        <Button
                            onClick={() => this.props.deleteStream(id)}
                            to='/'
                        >
                            Delete
                        </Button>
                        <Button cancel='true' to='/'>
                            Cancel
                        </Button>
                    </div>
                </div>
            </Div>
        )
    }

    render() {
        console.log(this.props)
        return <Modal>{this.renderModal()}</Modal>
    }
}

const mapStateToProps = (state, ownProps) => {
    return {
        stream: state.streams.data[ownProps.match.params.id]
    }
}

export default connect(
    mapStateToProps,
    { deleteStream }
)(StreamDelete)
