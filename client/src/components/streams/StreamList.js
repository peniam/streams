import React, { Component } from 'react'
import { connect } from 'react-redux'
import { fetchStreams } from '../../actions/streams'
import { Link } from 'react-router-dom'
import Loader from '../Loader/Loader'
import styled, { css } from 'styled-components'

const StyledLink = styled(Link)`
    background-color: #00f;
    color: #fff;

    display: inline-block;
    text-align: center;
    font-size: 1rem;
    font-weight: bold;

    padding: 0.5em 1.5em;
    border-radius: 0.5em;

    ${props =>
        props.danger &&
        css`
            background-color: red;
            margin-left: 0.5em;
        `}

    ${props =>
        props.create &&
        css`
            background-color: #5aa4d0;
            float: right;
            font-size: 1.3rem;
            padding: 0.5em;
        `}
`

const Li = styled.li`
    list-style: none;
    margin: 1.5em 0;
    display: flex;
    justify-content: space-between;
    align-items: center;
    border-bottom: 1px solid var(--main-color);

    & div:first-child > a {
        font-size: 0.8rem;
        padding: 0.25em 0;
        display: inline-block;
        color: brown;
        text-transform: uppercase;
    }
    p {
        font-size: 1.25rem;

        &::first-letter {
            text-transform: uppercase;
        }
    }
`

const H1 = styled.h1`
    font-size: 2rem;
`

class ListStreams extends Component {
    componentDidMount() {
        this.props.fetchStreams()
    }

    renderAdmin(id) {
        return (
            <div>
                <StyledLink to={`/streams/edit/${id}`}>Edit</StyledLink>
                <StyledLink danger='true' to={`/streams/delete/${id}`}>
                    Delete
                </StyledLink>
            </div>
        )
    }

    renderStreams = () => {
        const { streams } = this.props

        return Object.values(streams).map(
            ({ title, id, description, userId }) => {
                return (
                    <Li key={id}>
                        <div>
                            <Link to={`/streams/${id}`}>{title}</Link>
                            <p>{description}</p>
                        </div>

                        {userId === this.props.userId
                            ? this.renderAdmin(id)
                            : null}
                    </Li>
                )
            }
        )
    }
    render() {
        const { loading, isSignIn } = this.props
        if (loading) {
            return <Loader />
        }

        return (
            <div>
                <H1>Streams</H1>
                <ul>{this.renderStreams()}</ul>
                {isSignIn && (
                    <StyledLink create='true' to='/streams/new'>
                        Create Stream
                    </StyledLink>
                )}
            </div>
        )
    }
}

const mapStateToProps = state => {
    return {
        streams: state.streams.data,
        loading: state.streams.loading,
        userId: state.auth.userId,
        isSignIn: state.auth.isSignIn
    }
}

export default connect(
    mapStateToProps,
    { fetchStreams }
)(ListStreams)
