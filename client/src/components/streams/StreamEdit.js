import React, { Component } from 'react'
import StreamForm from './StreamForm'
import { connect } from 'react-redux'
import { fetchStream, editStream } from '../../actions/streams'
import Loader from '../Loader/Loader'

class StreamEdit extends Component {
    componentDidMount() {
        this.props.fetchStream(this.props.match.params.id)
    }

    onSubmit = formValues => {
        const id = this.props.match.params.id
        this.props.editStream(id, formValues)
    }
    render() {
        if (!this.props.stream) {
            return <Loader />
        }
        const { title, description } = this.props.stream

        return (
            <div>
                <h3>Edit a Stream</h3>
                <StreamForm
                    onSubmit={this.onSubmit}
                    initialValues={{ title, description }}
                />
            </div>
        )
    }
}

const mapStateToProps = (state, ownProps) => {
    return {
        stream: state.streams.data[ownProps.match.params.id]
    }
}

export default connect(
    mapStateToProps,
    { editStream, fetchStream }
)(StreamEdit)
