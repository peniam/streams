import React, { Component, Fragment } from 'react'
// import { hot } from 'react-hot-loader/root'
import { Router, Route, Switch } from 'react-router-dom'
import history from '../history'

import Header from './Header'
import StreamList from './streams/StreamList'
import StreamCreate from './streams/StreamCreate'
import StreamShow from './streams/StreamShow'
import StreamEdit from './streams/StreamEdit'
import StreamDelete from './streams/StreamDelete'
import styled, { createGlobalStyle } from 'styled-components'
import reset from '../constants/reset'
const GlobalStyle = createGlobalStyle`${reset}`

const Div = styled.ul`
    max-width: 800px;
    margin: 0 auto;
    display: flex;
    flex-direction: column;
    padding: 1.5em;
`

class App extends Component {
    render() {
        return (
            <div>
                <Router history={history}>
                    <Fragment>
                        <GlobalStyle />
                        <Header />
                        <Div>
                            <Switch>
                                <Route path='/' exact component={StreamList} />
                                <Route
                                    path='/streams/new'
                                    exact
                                    component={StreamCreate}
                                />
                                <Route
                                    path='/streams/:id'
                                    exact
                                    component={StreamShow}
                                />
                                <Route
                                    path='/streams/edit/:id'
                                    exact
                                    component={StreamEdit}
                                />
                                <Route
                                    path='/streams/delete/:id'
                                    exact
                                    component={StreamDelete}
                                />
                            </Switch>
                        </Div>
                    </Fragment>
                </Router>
            </div>
        )
    }
}

export default App
