import React, { Component } from 'react'
import GoogleAuth from './GoogleAuth'
import { Link } from 'react-router-dom'
import styled from 'styled-components'
import ErrorBoundary from './ErrorBoundary'

const StyledLink = styled(Link)`
    color: #fff;
    font-weight: bold;
    padding: 0.5em;
    font-size: 1.25rem;

    &:first-of-type {
        margin-right: auto;
        padding-left: 0.5em;
    }
`

const Div = styled.div`
    background-color: #79340b;
    padding: 0.25em 0.5em;

    & > div:first-child {
        max-width: 1200px;
        margin: 0 auto;
        display: flex;
        justify-content: center;
        align-items: center;
    }
`

class Header extends Component {
    render() {
        return (
            <Div>
                <div>
                    <StyledLink to='/'>Streamy</StyledLink>
                    <StyledLink to='/'>All Streams</StyledLink>
                    <ErrorBoundary>
                        <GoogleAuth />
                    </ErrorBoundary>
                </div>
            </Div>
        )
    }
}

export default Header
