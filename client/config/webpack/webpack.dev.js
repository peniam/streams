/* global module require  */
const webpack = require('webpack')
const Dotenv = require('dotenv-webpack')

module.exports = {
    mode: 'development',
    devtool: 'cheap-eval-source-map',
    devServer: {
        contentBase: './dist',
        open: true,
        overlay: true,
        hot: true,
        historyApiFallback: true
    },
    module: {
        rules: [
            {
                test: /\css$/,
                exclude: /\.module\.css$/,
                use: ['style-loader', 'css-loader']
            },
            {
                test: /\.module\.css$/,
                use: [
                    {
                        loader: 'style-loader'
                    },
                    {
                        loader: 'css-loader',
                        options: {
                            modules: true,
                            sourceMap: true
                        }
                    }
                ]
            }
        ]
    },
    plugins: [new webpack.HotModuleReplacementPlugin(), new Dotenv()]
}
