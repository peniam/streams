/* global module */
module.exports = function(api) {
    api.cache(true)
    const presets = ['@babel/preset-env', '@babel/preset-react']

    const plugins = [
        'react-hot-loader/babel',
        'babel-plugin-styled-components',
        '@babel/plugin-proposal-class-properties',
        '@babel/plugin-proposal-object-rest-spread',
        '@babel/plugin-transform-runtime'
    ]

    return {
        presets,
        plugins
    }
}
