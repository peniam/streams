/* global require module process */
const base = require('./config/webpack/webpack.base');
const merge = require('webpack-merge');

const envs = {
    development: 'dev',
    production: 'prod'
};

const env = envs[process.env.NODE_ENV || 'development'];
const envConfig = require(`./config/webpack/webpack.${env}`);
module.exports = merge(base, envConfig);
